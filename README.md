# Docker image macodigo/rust-nodejs-macodigo

### Configuration
* Debian Buster
* Rust
* Node.js


### Docker Hub
Avaialble at, https://hub.docker.com/r/macodigo/rust-nodejs-macodigo/


### pre-cached packages
Rust and NPM packages are pre-cached.  Configuration can be updated in
Cargo.toml and package.json according to the projrect requirements
