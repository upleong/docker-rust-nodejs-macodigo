all: build

build:
	docker build -t macodigo/rust-nodejs-macodigo .

push:
	docker push macodigo/rust-nodejs-macodigo

bash:
	docker run -it macodigo/rust-nodejs-macodigo /bin/bash
