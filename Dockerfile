# Builder for Rust (BE) + Javascript (FE)
FROM debian:buster-slim
MAINTAINER UP Leong <pio.leong@gmail.com>

# Update apt
RUN apt-get update

# Install utilities
RUN apt-get install -y curl git xz-utils build-essential pkg-config libssl-dev


# Install Rust
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y
ENV PATH /root/.cargo/bin:$PATH
RUN . /root/.cargo/env && rustc --version
##
RUN echo '[target.armv7-unknown-linux-gnueabihf]' >> /root/.cargo/config
RUN echo 'linker = "arm-linux-gnueabihf-gcc"' >> /root/.cargo/config
RUN echo '[target.armv7-unknown-linux-musleabihf]' >> /root/.cargo/config
RUN echo 'linker = "arm-linux-musleabihf-gcc"' >> /root/.cargo/config
##
RUN rustup target add x86_64-unknown-linux-musl
RUN rustup target add armv7-unknown-linux-musleabihf
RUN curl https://musl.cc/arm-linux-musleabihf-cross.tgz | tar -xz -C /opt
ENV PATH $PATH:/opt/arm-linux-musleabihf-cross/bin/
##
RUN cargo install cargo-binutils
RUN rustup component add llvm-tools-preview


# Install nodejs via NVM
ENV NODE_VERSION 10.16.3
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
ENV NVM_DIR /root/.nvm
RUN . $NVM_DIR/nvm.sh && nvm install $NODE_VERSION
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH
RUN node --version


# Cleanup Apt
RUN apt-get clean
#RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


#### Prepare NPM packages in node_modules
WORKDIR /usr/src/app-fe
ADD package.json /usr/src/app-fe/package.json
RUN npm install


#### Prepare Rust packages
WORKDIR /usr/src/app
ADD Cargo.toml ./Cargo.toml
RUN mkdir -p ./src && echo "// dummy file" > ./src/lib.rs
#### RUN cargo build -j3
RUN cargo build --release -j3
RUN cargo build --release --target x86_64-unknown-linux-musl -j3
RUN cargo build --release --target armv7-unknown-linux-musleabihf -j3


# Er, sure, why not
CMD ["/bin/bash"]
